<!-- 2024/2/23 1 UPDATE -->
# 傅卿何 plugin index
个人Plugin索引，以及其他内容  
本仓库为[Yunzai-Bot V3](https://gitee.com/Le-niao/Yunzai-Bot) & [A-Yunzai](https://gitee.com/ningmengchongshui/azai-bot)提供插件  
如出现插件报错或功能不适配等情况请及时联系[作者](https://gitee.com/Tloml-Starry)或在相应插件地址发起issues

## 联系我
点开我的主页私信我
 & [点击跳转我的QQ](https://qm.qq.com/q/h9UHpRmomc)
 & [点击跳转我的QQ群](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=5YqRg5S_pk_oVolSp7wydrx2ZFblhE-U&authKey=rl1zGbz8KHpvTlgkd%2BDm5Z9y%2FdMaWFnIL9p0FNVS6dyNmkNQ%2F2ngy3oU5Gynx8Ob&noverify=0&group_code=392665563)

---

## Plugin 插件
<details>
<summary>点击展开 · Sky光遇插件</summary>

##### 插件原地址 [点我跳转](https://gitee.com/Tloml-Starry/Tlon-Sky)
##### 介绍
为[Yunzai-Bot V3](https://gitee.com/Le-niao/Yunzai-Bot)提供光遇相关功能,查询每日任务,排队状态,等...安装后可发送`光遇菜单`查看更多内容
##### 安装命令
```
git clone https://gitee.com/Tloml-Starry/Tlon-Sky.git ./plugins/Tlon-Sky/
```
</details>
<details>
<summary>点击展开 · 曲奇点点乐</summary>

##### 插件原地址 [点我跳转](https://gitee.com/Tloml-Starry/Cookie-plugin)
##### 介绍
挂机类游戏 网页体验 《饼干点点乐》是一款制作饼干，数量大到夸张的小游戏。为了制作更多的饼干，你可以招兵买马各式各样的饼干制造者，比如慈祥的老奶奶，农场，工厂和异世界的穿越门等...安装后可发送`曲奇菜单`查看更多内容
##### 安装命令
```
git clone https://gitee.com/Tloml-Starry/Cookie-plugin.git ./plugins/Cookie-plugin/
```
</details>
<details>
<summary>点击展开 · 蛋仔抽奖模拟器</summary>

##### 插件原地址 [点我跳转](https://gitee.com/Tloml-Starry/Eggy)
##### 介绍
提供蛋仔派对抽盲盒模拟功能，安装后可发送`蛋仔菜单`查看更多内容
##### 安装命令
```
git clone https://gitee.com/Tloml-Starry/Eggy.git ./plugins/Eggy/
```
</details>
<details>
<summary>点击展开 · 卿何插件</summary>

##### 插件原地址 [点我跳转](https://gitee.com/Tloml-Starry/qinghe-plugin)
##### 介绍
为[Yunzai-Bot V3](https://gitee.com/Le-niao/Yunzai-Bot)提供一些娱乐小功能
##### 安装命令
```
git clone https://gitee.com/Tloml-Starry/qinghe-plugin.git ./plugins/qinghe-plugin/
```
</details>

## example 插件
<details>
<summary>点击展开 · example</summary>

#### [插件地址,点我跳转](https://gitee.com/Tloml-Starry/Plugin-Example)
 - 刷听歌时长
 - 抖音AI图生图
 - 姓名测算
 - 发言榜
 - 碧蓝档案高仿文字图
 - 手机号估价
 - 塔罗牌
 - New Bing
 - 狗屁不通
 - 文字找茬
 - 答案之书
 - 表情包搜索
 - 抽象网名生成
 - Steam在线游戏人数排行
 - Epic免费游戏
 - 福布斯全球富豪榜
 - 手机处理器排行榜
 - 手机跑分排行榜
 - QQ信息查询
</details>

## Resource 资源
<details>
<summary>点击展开 · Sky复刻图库</summary>

##### 资源原地址 [点我跳转](https://gitee.com/Tloml-Starry/Tlon-Sky-reprint)
##### 介绍
用于更新Tlon-Sky插件复刻图
##### 安装命令
安装[Sky光遇插件](https://gitee.com/Tloml-Starry/Tlon-Sky)后使用指令`#复刻兑换图`即可
</details>
<details>
<summary>点击展开 · Sky绘画分享图库</summary>

##### 资源原地址 [点我跳转](https://gitee.com/Tloml-Starry/Tlon-Sky-gallery)
##### 介绍
用于安装Tlon-Sky插件绘画分享图库
##### 安装命令
安装[Sky光遇插件](https://gitee.com/Tloml-Starry/Tlon-Sky)后使用指令`#Sky更新图库`即可
</details>